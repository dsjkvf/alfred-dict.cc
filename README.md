dict.cc
=======

## About

dict.cc is a [workflow](https://www.alfredapp.com/help/workflows/) for [Alfred 2](https://www.alfredapp.com/), which uses [dict.cc website](http://dict.cc) to provide the requested translations. dict.cc is mostly a lightly improved version of [alfred-dict.cc-workflow/](https://github.com/elkatwork/alfred-dict.cc-workflow/) by [Thomas Hirsch](https://github.com/elkatwork), and therefore all the credits and thanks should go directly to him.

This repository contains only the modified version of the Python script (`dict.cc.py`), which queries [dict.cc website](http://dict.cc) and produces the output needed by `Alfred 2`. The bundled workflow can be downloaded [here](https://bitbucket.org/dsjkvf/alfred-dict.cc/downloads/dict_cc.alfredworkflow).

