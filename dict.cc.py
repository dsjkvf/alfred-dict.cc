#!/usr/bin/env python
# -*- coding: utf-8 -*-

# HEADER

# Details
__title__      = "Alfred Workflow: dict.cc"
__author__     = "elkatwork"
__maintainer__ = "dsjkvf"
__url__        = "https://bitbucket.org/dsjkvf/alfred-dict.cc"
__copyright__  = "elkatwork"
__credits__    = ["elkatwork", "raaapha"]
__version__    = "1.0.1"
__status__     = "Production"

# MODULES

# System

import urllib2, urllib
import re
import sys
import string

# GLOBALS

# Preferences
# the number of results
MAX_RESULTS = 20

# OBJECTS

# Dictionary
class Dict:
    def __init__(self):
        self.OR = []
        self.TR = []

    def getResponse(self, dict, word):
        # Trick to avoid dict.cc from denying the request: change User-agent to Firefox's
        req = urllib2.Request("http://" + dict + ".dict.cc/?s=" + word, None, {'User-agent': 'Mozilla/6.0'})
        try:
            f = urllib2.urlopen(req)
            self.Response = f.read()
        except urllib2.HTTPError as err:
            # If wrong arguments provided or a translation wasn't found
            self.Response = ""

    # Find 'var c1Arr' and 'var c2Arr'
    def parseResponse(self, side):
        self.orig_words = []
        self.tran_words = []

        left_line = rght_line = ""

        # Split lines
        lines = self.Response.split("\n")

        for l in lines:
            if l.find("var c1Arr") >= 0:
                left_line =  l
            elif l.find("var c2Arr") >= 0:
                rght_line = l

        if not left_line or not rght_line:
            return False

        pattern = "\"[^,]+\""

        # Return list of matching strings
        if side == 1:
            self.orig_words = map(self.sanitizeWord, re.findall(pattern, left_line))
            self.tran_words = map(self.sanitizeWord, re.findall(pattern, rght_line))
        elif side == 2:
            self.orig_words = map(self.sanitizeWord, re.findall(pattern, rght_line))
            self.tran_words = map(self.sanitizeWord, re.findall(pattern, left_line))

    def getOutputLength(self):
        # Get minumum number of both eng and de
        minWords = len(self.orig_words) if len(self.orig_words) <= len(self.tran_words) else len(self.tran_words)

        # Is it more than MAX_RESULTS?
        minWords = minWords if minWords <= MAX_RESULTS else MAX_RESULTS

        # Find biggest word in first col
        length = 0
        for w in self.orig_words[:minWords]:
            length = length if length > len(w) else len(w)

        return length, minWords

    def printXMLResults(self, expression):
        print "<?xml version=\"1.0\"?>"
        print "<items>"

        if not self.orig_words or not self.tran_words:
            print "<item valid=\"no\">"
            print "<title>'%s' not found</title>" % string.strip(expression)
            print "<icon>rezu.png</icon>"
            print "</item>"

        else:
            length, minWords = self.getOutputLength()

            for word_idx in range(minWords):
                if self.orig_words[word_idx] == "\"\"": continue
                print "<item valid=\"yes\" arg=\"%s\">" % self.orig_words[word_idx]
                print "<title>%s</title>" % self.orig_words[word_idx]
                print "<subtitle>%s</subtitle>" % self.tran_words[word_idx]
                print "<icon>rezu.png</icon>"
                print "</item>"

        print "</items>"

    def sanitizeWord(self, word):
        word = word.replace("\\", "")
        return word.strip("\" ")


# MAIN

# Define
def main():
    if len(sys.argv) < 4:
        print "USAGE: dict.cc <DICTIONARY-TO-USE (e.g., ende)> <COLUMN (1 or 2)> <TERM-TO-TRANSLATE>"
        return
    dictionary = sys.argv[1]
    which_side = int(sys.argv[2])
    expression = ""
    for index in range(3, len(sys.argv)):
        expression += sys.argv[index] + " "

    myDict = Dict()
    myDict.getResponse(dictionary, urllib.quote(expression))
    myDict.parseResponse(which_side)
    myDict.printXMLResults(expression)
    return

# Run
if __name__ == "__main__":
    main()
